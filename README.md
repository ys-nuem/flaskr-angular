# Flaskr with AngualrJS

AngularじゃなくてKnockout使うかも

## Requirements

* Python (with virtualenv)
* bower (management of client-side dependencies)
* gulp (build typescript)

```shell-session
$ virtualenv --python=$(which python) env
$ npm install -g bower gulp-cli
$ # or
$ npm install bower gulp-cli && export PATH=$(realpath node_modules/.bin):$PATH
$ source env/Scripts/activate  # Windows
$ source env/bin/activate      # *nix
$ python -mpip install pip --upgrade
$ pip install -r requirements.txt
```

## Setup

```shell-session
$ cd flaskr-angular/
$ bower install
```

