-- vim: set ts=2 sw=2 et :

drop table if exists entries;

create table entries(
    id    integer primary key autoincrement
  , title text not null
  , text  text not null
);
