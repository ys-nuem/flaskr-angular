#!/usr/bin/env python
# vim: set fileencoding=utf-8

import sqlite3
import flask
from contextlib import closing


DATABASE    = 'flaskr.db'
DEBUG       = True
SECRET_KEY  = 'development key'
USERNAME    = 'admin'
PASSWORD    = 'default'


# create our little application :)
app = flask.Flask(__name__)
app.config.from_object(__name__)
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


def connect_db():
    return sqlite3.connect(app.config['DATABASE'])


def init_db():
    with closing(connect_db()) as db:
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


@app.before_request
def before_request():
    flask.g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    db = getattr(flask.g, 'db', None)
    if db is not None:
        db.close()


@app.route('/')
def show_entries():
    cur = flask.g.db.execute('select title,text from entries order by id desc')
    entries = [dict(title=row[0], text=row[1]) for row in cur.fetchall()]
    return flask.render_template('show_entries.html', entries=entries)


@app.route('/add', methods=['POST'])
def add_entry():
    if not flask.session.get('logged_in'):
        flask.abort(401)
    flask.g.db.execute('insert into entries (title,text) values (?,?)',
                       [flask.request.form['title'], flask.request.form['text']])
    flask.g.db.commit()
    flask.flash('New entry was successfully posted')
    return flask.redirect(flask.url_for('show_entries'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if flask.request.method = 'POST':
        if flask.request.form['username'] != app.config['USERNAME']
            error = 'Invalid username'
        elif flask.request.form['password'] != app.config['PASSWORD']
            error = 'Invalid password'
        else:
            flask.session['logged_in'] = True
            flask.flash('You were logged in')
            return flask.redirect(flask.url_for('show_entries'))
    return flask.render_template('login.html', error=error)


@app.route('/logout')
def logout():
    flask.session.pop('logged_in', None)
    flask.flash('You were logged out')
    return flask.redirect(flask.url_for('show_entries'))

    
if __name__ == '__main__':
    app.run(debug=True)
